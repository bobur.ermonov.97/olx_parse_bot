import json
import sys

from telebot import TeleBot, types
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.bot.models import TelegramUsers

TOKEN = "5804497660:AAGdyNQ9EWhZXLqv3oJ3B3-vyDlqg-T8PT8"

bot = TeleBot(TOKEN)


class BotUpdate(APIView):
    def post(self, request, *args, **kwargs):
        # Сюда должны получать сообщения от телеграм и далее обрабатываться ботом
        t_data = json.loads(request.body)
        json_str = request.body.decode('UTF-8')
        update = types.Update.de_json(json_str)
        bot.process_new_updates([update])

        return Response({'code': 200})


@bot.message_handler(commands=['start'])
def start_message(message):
    telegram_user = TelegramUsers.objects.all().filter(chat_id=message.from_user.id).first()

    if not telegram_user:
        try:
            print(message.from_user)
            # phone = message.contact.phone_number
            name = message.from_user.first_name
            last_name = message.from_user.last_name
            user_name = message.from_user.username
            chat_id = message.from_user.id
            language = message.from_user.language_code

            telegram_user = TelegramUsers.objects.create(
                name=name,
                last_name=last_name,
                user_name=user_name,
                chat_id=chat_id,
                language=language

            )
            text = "Assalomu alaykum botga xush kelibsiz!"
        except Exception as e:
            text = str(e)
    else:
        try:
            language = message.from_user.language_code

            telegram_user.language = language
            telegram_user.save()
            text = "Assalomu alaykum botga xush kelibsiz!"
        except:
            text = "error2"

    bot.send_message(message.chat.id, text=text, parse_mode='HTML')


def getUser():
    chat_id = TelegramUsers.objects.filter(status=True, block_bot=True).all().values_list('chat_id', 'language')

    return chat_id


def post_message_to_telegram(text):
    users = getUser()
    try:
        for user in users:
            bot.send_photo(user[0],
                           photo='https://waqi.info/icons/logo.png?_=1669089706',
                           caption=text, parse_mode='HTML')
        respon = 'yes'


    except:
        print("Unexpected error:", sys.exc_info()[0])
        respon = str(sys.exc_info()[0])

    return respon


# https://api.telegram.org/bot5804497660:AAGdyNQ9EWhZXLqv3oJ3B3-vyDlqg-T8PT8/setWebhook?url=https://5192-185-213-229-63.in.ngrok.io/api/v1/bot/
def send_message(instance=None):
    users = getUser()
    text = '🏢🏢🏢🏢🏢🏢🏢\n'
    for user in users:
        if instance:
            text += instance.joylangan_vaqt + '\n'
            text += instance.title + '\n'
            text += instance.price + '\n'
            text += instance.uy_qavatliligi + '\n'
            text += instance.room_count + '\n'
            text += instance.m_2x2 + '\n'
            text += instance.qavat + '\n'
            text += instance.desc + '\n'
            text += 'https://www.olx.uz' + instance.slug + '\n'
        bot.send_message(user[0],
                         # photo='https://waqi.info/icons/logo.png?_=1669089706',
                         text=text, parse_mode='HTML')
