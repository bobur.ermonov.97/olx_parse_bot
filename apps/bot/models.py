from django.db import models


class TelegramUsers(models.Model):
    name = models.CharField(max_length=50, null=True, blank=True)
    last_name = models.CharField(max_length=50, null=True, blank=True)
    user_name = models.CharField(max_length=50, null=True, blank=True)
    chat_id = models.CharField(max_length=50)
    status = models.BooleanField(default=True)
    register_status = models.BooleanField(default=False)
    phone = models.CharField(max_length=50, null=True, blank=True)
    language = models.CharField(max_length=10, null=True)
    block_bot = models.BooleanField(default=True, null=True, blank=True)

    def __str__(self):
        return str(self.name)


class House(models.Model):
    title = models.CharField(max_length=1024)
    price = models.CharField(max_length=1024, blank=True, null=True)
    is_sale_status = models.BooleanField(default=False)
    room_count = models.CharField(max_length=1024, blank=True, null=True)
    m_2x2 = models.CharField(max_length=1024, blank=True, null=True)
    qavat = models.CharField(max_length=1024, blank=True, null=True)
    uy_qavatliligi = models.CharField(max_length=1024, blank=True, null=True)
    slug = models.CharField(max_length=1024, blank=True, null=True)
    link = models.URLField(blank=True, null=True)
    desc = models.TextField(blank=True, null=True)
    joylangan_vaqt = models.CharField(max_length=1024,blank=True, null=True)
    created_at = models.DateTimeField('Craeted date', auto_now_add=True)
    updated_at = models.DateTimeField('Update date', auto_now=True)

    def __str__(self):
        return str(self.title)



