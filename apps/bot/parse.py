from django.http import HttpResponse
from rest_framework.decorators import api_view
from rest_framework.response import Response

from apps.bot.models import House
from apps.bot.views import post_message_to_telegram, send_message


def parser_aqicn():
    import requests
    from bs4 import BeautifulSoup

    link = 'https://aqicn.org/city/uzbekistan/tashkent/us-embassy/'
    response = requests.get(link).text
    soup = BeautifulSoup(response, 'lxml')
    div_maincityview = soup.find('div', id='maincityview')
    div_aqiwgtvalue = div_maincityview.find('div', id='aqiwgtvalue')
    post_message_to_telegram(div_aqiwgtvalue.text)
    print(div_aqiwgtvalue.text)


@api_view(['GET'])
def parser_cbu_view(request):
    import requests
    from bs4 import BeautifulSoup
    links = [
        'https://www.olx.uz/d/oz/nedvizhimost/kvartiry/prodazha/q-ganga/?currency=UZS&search%5Bfilter_float_price:from%5D=500000000&search%5Bfilter_float_price:to%5D=900000000',
        'https://www.olx.uz/d/oz/nedvizhimost/kvartiry/prodazha/q-sebzor/?currency=UZS&search%5Bfilter_float_price:from%5D=500000000&search%5Bfilter_float_price:to%5D=900000000',
        'https://www.olx.uz/d/oz/nedvizhimost/kvartiry/prodazha/tashkent/?search%5Bdistrict_id%5D=25&search%5Bfilter_float_price:from%5D=400000000&search%5Bfilter_float_price:to%5D=6000000000&search%5Bfilter_float_number_of_rooms:from%5D=2&search%5Bfilter_float_number_of_rooms:to%5D=3&search%5Bfilter_enum_type_of_market%5D%5B0%5D=secondary&currency=UZS',
        'https://www.olx.uz/d/oz/nedvizhimost/kvartiry/prodazha/tashkent/?search%5Bdistrict_id%5D=26&search%5Bfilter_float_price%3Afrom%5D=400000000&search%5Bfilter_float_price%3Ato%5D=6000000000&search%5Bfilter_float_number_of_rooms%3Afrom%5D=2&search%5Bfilter_float_number_of_rooms%3Ato%5D=3&search%5Bfilter_enum_type_of_market%5D%5B0%5D=secondary&currency=UZS',
        'https://www.olx.uz/d/oz/nedvizhimost/kvartiry/prodazha/tashkent/?search%5Bdistrict_id%5D=24&search%5Bfilter_float_price:from%5D=400000000&search%5Bfilter_float_price:to%5D=6000000000&search%5Bfilter_float_number_of_rooms:from%5D=2&search%5Bfilter_float_number_of_rooms:to%5D=3&search%5Bfilter_enum_type_of_market%5D%5B0%5D=secondary&currency=UZS',
        'https://www.olx.uz/d/oz/nedvizhimost/kvartiry/prodazha/tashkent/?search%5Bdistrict_id%5D=20&search%5Bfilter_float_price:from%5D=400000000&search%5Bfilter_float_price:to%5D=6000000000&search%5Bfilter_float_number_of_rooms:from%5D=2&search%5Bfilter_float_number_of_rooms:to%5D=3&search%5Bfilter_enum_type_of_market%5D%5B0%5D=secondary&currency=UZS'
    ]
    for jtem in range(1, 20, 1):
        print("page:", jtem)
        # link = 'https://www.olx.uz/d/oz/nedvizhimost/kvartiry/prodazha/?currency=UZS&search%5Bfilter_float_price:from%5D=400000000&search%5Bfilter_float_price:to%5D=600000000&search%5Bfilter_float_number_of_rooms:from%5D=2&search%5Bfilter_float_number_of_rooms:to%5D=3&search%5Bfilter_enum_type_of_market%5D%5B0%5D=secondary'
        for link in links:
            try:
                if jtem != 1:
                    link += f'&page={jtem}'
                response = requests.get(link).text
                soup = BeautifulSoup(response, 'lxml')
                class_css_pband8 = soup.find('div', class_='css-pband8')
                class_l_card = class_css_pband8.find_all('div', {"data-cy": "l-card"})
                for item in class_l_card:
                    link_href = item.find('a', class_='css-rc5s2u').get('href')
                    location_date = item.find('p', {'data-testid': 'location-date'})
                    if location_date:
                        location_date = location_date.text
                        # print(location_date)
                    link_inline = "https://www.olx.uz" + link_href
                    response = requests.get(link_inline).text
                    soup = BeautifulSoup(response, 'lxml')
                    uy_qavat = ''
                    m_2x2 = ''
                    room_count = ''
                    qavat = ''
                    title = ''
                    desc = ''
                    price = ''
                    try:

                        class_div_css_1wws9er = soup.find('div', class_='css-1wws9er')
                        title = class_div_css_1wws9er.find('h1', {'data-cy': 'ad_title'}).text
                        # print(title)
                        desc = class_div_css_1wws9er.find('div', {'data-cy': 'ad_description'}).text
                        # print(desc)
                        price = class_div_css_1wws9er.find('div', {'data-testid': 'ad-price-container'}).text
                        # print(price)

                        x = class_div_css_1wws9er.find('ul', class_='css-sfcl1s').find_all('li', class_='css-1r0si1e')

                        for j in x:
                            if (j.text[:8] == 'Uy qavat') and j.text[:17] == "Uy qavatliligi: 9":
                                uy_qavat = j.text

                            elif (j.text[:13] == 'Umumiy maydon'):
                                m_2x2 = j.text
                            elif (j.text[:5] == 'Qavat'):
                                qavat = j.text
                            elif (j.text[:12] == 'Xonalar soni'):
                                room_count = j.text

                        # print(x)
                        if uy_qavat:
                            house = House.objects.filter(slug=link_href).first()
                            # print(house)
                            if not house:
                                house = House()
                                house.title = str(title)
                                house.price = str(price)
                                house.uy_qavatliligi = str(uy_qavat)
                                house.room_count = str(room_count)
                                house.m_2x2 = str(m_2x2)
                                house.qavat = str(qavat)
                                house.slug = link_href
                                house.link = link_inline
                                house.desc = str(desc)
                                house.joylangan_vaqt = str(location_date)
                                house.save()
                                send_message(house)

                    except Exception as e:
                        print(str(e))
            except Exception as e:
                print(str(e))

    res = {"msg", 'post_message_to_telegram(class_css_pband8.text)'}
    return Response(res)
