from django.contrib import admin

# Register your models here.
from apps.bot.models import *

admin.site.register(TelegramUsers)


@admin.register(House)
class HouseAdmin(admin.ModelAdmin):
    list_display = ('joylangan_vaqt', 'title', 'price', 'qavat', 'uy_qavatliligi', 'link', 'created_at')
    ordering = ('-created_at',)