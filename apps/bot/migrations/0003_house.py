# Generated by Django 4.1.3 on 2022-12-05 08:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bot', '0002_alter_telegramusers_status'),
    ]

    operations = [
        migrations.CreateModel(
            name='House',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=1024)),
                ('price', models.FloatField(blank=True, null=True)),
                ('is_sale_status', models.BooleanField(default=False)),
                ('room_count', models.IntegerField(blank=True, null=True)),
                ('m_2x2', models.IntegerField(blank=True, null=True)),
                ('qavat', models.IntegerField(blank=True, null=True)),
                ('uy_qavatliligi', models.IntegerField(blank=True, null=True)),
                ('slug', models.SlugField(blank=True, null=True)),
                ('desc', models.TextField(blank=True, null=True)),
                ('joylangan_vaqt', models.DateTimeField(blank=True, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Craeted date')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='Update date')),
            ],
        ),
    ]
