# Generated by Django 4.1.3 on 2022-12-05 09:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bot', '0005_alter_house_m_2x2_alter_house_price_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='house',
            name='joylangan_vaqt',
            field=models.CharField(blank=True, max_length=1024, null=True),
        ),
    ]
