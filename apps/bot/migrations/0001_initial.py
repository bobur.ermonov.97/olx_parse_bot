# Generated by Django 4.1.3 on 2022-11-22 04:58

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='TelegramUsers',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=50, null=True)),
                ('last_name', models.CharField(blank=True, max_length=50, null=True)),
                ('user_name', models.CharField(blank=True, max_length=50, null=True)),
                ('chat_id', models.CharField(max_length=50)),
                ('status', models.BooleanField(default=False)),
                ('register_status', models.BooleanField(default=False)),
                ('phone', models.CharField(blank=True, max_length=50, null=True)),
                ('language', models.CharField(max_length=10, null=True)),
                ('block_bot', models.BooleanField(blank=True, default=True, null=True)),
            ],
        ),
    ]
